# xmr-stak-rx-unit

XMR-STAK-RX systemd unit file with as many systemd sandboxing options enabled as possible.

# Installation

Examples are for Debian 11.

1. Clone the repo and copy the unit file to the systemd unit files folder.
   For example:-
    ```
    git clone git@gitlab.com:ar2000jp/xmr-stak-rx-unit.git
    cd xmr-stak-rx
    sudo cp xmr-stak-rx.service /etc/systemd/system/
    ```
2. Download and extract the xmr-stak-rx binary to "/opt/xmr-stak-rx/".
   For example:-
   ```
   sudo mkdir -p /opt/xmr-stak-rx/
   tar -xvf xmr-stak-rx-linux-*-cpu.tar.xz
   cd xmr-stak-rx-linux-*-cpu
   sudo cp xmr-stak-rx /opt/xmr-stak-rx/
   sudo chmod a+x /opt/xmr-stak-rx/xmr-stak-rx
   ```
3. Create and copy xmr-stak-rx config files to the xmr-stak-rx systemd unit config folder (usually "/etc/xmr-stak-rx").
   For example:-
   ```
   sudo mkdir -p /etc/xmr-stak-rx/
   sudo cp cpu.txt /etc/xmr-stak-rx/
   sudo cp config.txt /etc/xmr-stak-rx/
   sudo cp pools.txt /etc/xmr-stak-rx/
   ```

4. Create "xmr-stak-rx" linux user.
   For example:-
   ```
   sudo useradd -M -U -r -d /opt/xmr-stak-rx/ -s /usr/sbin/nologin xmr-stak-rx
   ```

5. Ask systemd daemon to reload
   ```
   sudo systemctl daemon-reload
   ```

# Usage

- To start the miner, do:
   ```
   sudo systemctl start xmr-stak-rx.service
   ```

- To stop the miner, do:
    ```
    sudo systemctl stop xmr-stak-rx.service
    ```

- To enable miner start on boot, do:
    ```
    sudo systemctl enable xmr-stak-rx.service
    ```

- To see the miner logs, do:
   ```
   sudo journalctl -f --unit=xmr-stak-rx
   ```
